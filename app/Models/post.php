<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $table = 'post';
    protected $fillable = [
        'title', 'image','description','user_id'
    ]; 

    public function post_category()
    {
        return $this->hasManyThrough(
            'App\Models\category',
            'App\Models\post_category',
            'post_id',
            'id', 
            'id',
            'category_id',
        )->select('category.title');
    }
    public function user()
    {
    	return $this->belongsTo('App\User','user_id')->select('id','name');
    }
}
