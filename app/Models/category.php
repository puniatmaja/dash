<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'title', 'user_id',
    ];
    public function user()
    {
    	return $this->belongsTo('App\User','user_id')->select('id','name');
    }
}
