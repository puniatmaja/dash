<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\category;
use DB;
use Validator;
class CategoryController extends Controller
{
    public function view()
	{
	    return view('category.create');
	}

   	public function create(Request $req)
   	{
	   	$req->validate([
	        'title' => 'required|max:255'	       
	    ]);
	   
		
	   	$input = $req->input();
	   	$input['user_id'] = Auth::id();	   	
	   	category::create($input);

		$req->session()->flash('success','Success Make Category');
	   	return redirect('category');
	}

	public function edit($id)
	{
		$data['single'] = category::where('id',$id)->where('user_id',Auth::id())->first();		
	    return view('category.update')->with($data);
	}

	public function update(Request $req,$id)
   	{
	   	$req->validate([
	        'title' => 'required|max:255'	       
	    ]);

		$input = $req->input();
		$input['user_id'] = Auth::id();		

	   	category::find($id)->update($input);

		$req->session()->flash('success','Success Update Category');
	   	return redirect('category');
	}

	public function delete(Request $req)
	{		
	   	category::find($req->input('id'))->delete();		
		$req->session()->flash('success','Success Delete Category');
	   	return redirect('category');
	}

	public function store(Request $req)
	{
		$data['list'] = category::with('user')->orderBy('id','DESC')->paginate(25);
        return view('category.list')->with($data);		

	}
}
