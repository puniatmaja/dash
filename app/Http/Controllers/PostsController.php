<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\post;
use App\Models\post_category;
use App\Models\category;
use DB;

class PostsController extends Controller
{
	public function view()
	{
		$data['category'] = category::all();	
	    return view('post.create')->with($data);
	}

   	public function create(Request $req)
   	{
	   	$req->validate([
	        'title' => 'required|max:255',
	        'description' => 'required',
	        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	        'category' => 'required'
	    ]);
	  

		// image upload	
		$image = $req->file('image');
		$imgPath = 'image/'.date('Y-m').'/';
		if (!file_exists(public_path($imgPath))) {
		    mkdir('image/' . date('Y-m'), 0777);		   
		} 

		$image->move($imgPath,$image->getClientOriginalName());

	   	$input = $req->input();
	   	$input['user_id'] = Auth::id();
	   	$input['image'] = date('Y-m').'/'.$image->getClientOriginalName();

	   	$id = post::create($input);
	   	if (!empty($input['category'])) {
	   		foreach ($input['category'] as $value) {
	   			post_category::create([
	   				'post_id' => $id->id, 
	   				'category_id' => $value
	   			]);
	   		}
	   	}
		$req->session()->flash('success','Success Make Post');
	   	return redirect('post');
	}

	public function edit($id)
	{
		$data['single'] = post::where('id',$id)->where('user_id',Auth::id())->first();
		$post_category = DB::table('post_category')
								->join('category','post_category.category_id','=','category.id')
								->select('category.id','category.title')
								->where('post_category.post_id',$id)
								->get();
		$data['post_category'] = array();
		foreach ($post_category as $row) {
			array_push($data['post_category'], $row->id);
		}


		$data['category'] = category::all();
	    return view('post.update')->with($data);
	}
	public function update(Request $req,$id)
   	{
	   	$req->validate([
	        'title' => 'required|max:255',
	        'description' => 'required',
	        'category' => 'required'
	    ]);
		
		$input = $req->input();	

		if (!empty($req->file('image'))) {			
			// image upload	
			$image = $req->file('image');
			$imgPath = 'image/'.date('Y-m').'/';
			if (!file_exists(public_path($imgPath))) {
			    mkdir('image/' . date('Y-m'), 0777);		   
			}

			$image->move($imgPath,$image->getClientOriginalName());   	
		   	$input['image'] = date('Y-m').'/'.$image->getClientOriginalName();
		}

	   	post::find($id)->update($input);
	   	post_category::where('post_id',$id)->delete();
	   	if (!empty($input['category'])) {
	   		foreach ($input['category'] as $value) {
	   			post_category::create([
	   				'post_id' => $id, 
	   				'category_id' => $value
	   			]);
	   		}
	   	}
		$req->session()->flash('success','Success Update Post');
	   	return redirect('post');
	}

	public function delete(Request $req)
	{
	   	post::find($req->input('id'))->delete();
	   	post_category::where('post_id',$req->input('id'))->delete();
		$req->session()->flash('success','Success Delete Post');
	   	return redirect('post');		
	}

	public function store(Request $req)
	{
		$data['list'] = post::with('user','post_category')->orderBy('id','DESC')->paginate(25);		
        return view('post.list')->with($data);
	}
}
