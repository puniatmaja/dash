<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['auth']], function () {	
    Route::get('/', function () {
	    return view('home');
	});

    // category
	Route::get('category','CategoryController@store');
	Route::get('category/create','CategoryController@view');
	Route::post('category','CategoryController@create');
	Route::get('category/edit/{id}','CategoryController@edit');
	Route::put('category/edit/{id}','CategoryController@update');
	Route::delete('category','CategoryController@delete');

	// post
	Route::get('post','PostsController@store');
	Route::get('post/create','PostsController@view');
	Route::post('post','PostsController@create');
	Route::get('post/edit/{id}','PostsController@edit');
	Route::put('post/edit/{id}','PostsController@update');
	Route::delete('post','PostsController@delete');

	// user
	Route::get('user','UsersController@store');
	Route::get('user/create','UsersController@view');
	Route::post('user','UsersController@create');
	Route::get('user/edit/{id}','UsersController@edit');
	Route::put('user/edit/{id}','UsersController@update');
	Route::delete('user','UsersController@delete');
	Route::get('profile','UsersController@profile');

});



Route::get('login','UsersController@login')->name('login');
Route::get('logout','UsersController@logout');
Route::post('login','UsersController@authenticate');