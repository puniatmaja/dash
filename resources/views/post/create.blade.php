@extends('layout')
@section('content')
		<!-- Page Heading -->
		  
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">New Post</h1>           
		</div>
		<form action="{{url('post')}}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="row">			
		        <div class="col-lg-8">	         
					<div class="card shadow mb-4">
			        	<div class="card-header">
		                  <i class="fas fa-edit"></i> New Post
		                </div>
			            <div class="card-body">
			            	<div class="form-group">
			            		<label for="">Title</label>
			            		<input name="title" class="form-control" type="text" placeholder="Enter Title" value="{{ old('title') }}">
			            	</div>
			            	@if($errors->has('title'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('title')}}
                                </div>
                            @endif
			            	<div class="form-group">	
			            		<label for="">Description</label>
								<textarea placeholder="Enter Description" class="form-control" id="summernote" name="description"><?= old('description') ?></textarea>
			            	</div>
			            	@if($errors->has('description'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('description')}}
                                </div>
                            @endif
			            </div>
			        </div>
		    	</div>
		    	<div class="col-lg-4">
					<div class="card shadow mb-4">
						<div class="card-header">
		                  <i class="fas fa-image"></i> Publish
		                </div>
			            <div class="card-body">
							<div class="form-group text-center border-bottom-info rounded border" style="position: relative;">							
	  							<img id="cover" src="{{asset('img/blank.jpg')}}" style="max-width: 100%" />
								<label for="imgInp" class="btn btn-info btn-circle" style="position: absolute;bottom: 10px;right: 10px;cursor: pointer;">
								<i class="fas fa-plus"></i>
								</label>
							</div>
							<input type='file' id="imgInp" name="image" style="display: none;" />
							@if($errors->has('image'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('image')}}
                                </div>
                            @endif
							

			              	<div class="form-group">
			            		<label for="">Category</label>			            		
				            	<select class="form-control js-example-basic-multiple" name="category[]" multiple="multiple">
				            		@foreach($category as $row)
								  	<option value="{{$row->id}}">{{$row->title}}</option>
								  	@endforeach
								</select>
			            	</div>
			            	@if($errors->has('category'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('category')}}
                                </div>
                            @endif
			            	<div class="mt-4">
			            		<a href="{{url('post')}}" class="btn btn-light btn-icon-split">
				                    <span class="icon text-gray-600">
				                      <i class="fas fa-arrow-left"></i>
				                    </span>
				                    <span class="text">Cancel</span>
			                  	</a>
			            		<button type="submit" class="btn btn-success btn-icon-split float-right">
		                          <span class="icon text-white-50">
		                            <i class="fas fa-save"></i>
		                          </span>
		                          <span class="text">Save</span>
		                        </button>
			            	</div>

			            </div>
			        </div>
		    	</div>
			</div>			
		</form>

@endsection
@section('css')
	<link href="{{ asset('/') }}css/select2.min.css" rel="stylesheet" />
	<link href="{{ asset('/') }}vendor/summernote/summernote-bs4.css" rel="stylesheet">
    

@endsection

@section('js')
	<script src="{{ asset('/') }}js/select2.min.js"></script>
	<script src="{{ asset('/') }}vendor/summernote/summernote-bs4.js"></script>
	<script>
		$(document).ready(function() {
		    $('.js-example-basic-multiple').select2();
		});
		$('#summernote').summernote({
	        placeholder: 'Enter Description',
	        tabsize: 2,
	        height: 250
	      });

		function readURL(input) {
		  if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    
		    reader.onload = function(e) {
		      $('#cover').attr('src', e.target.result);
		    }
		    
		    reader.readAsDataURL(input.files[0]);
		  }
		}

		$("#imgInp").change(function() {
		  readURL(this);
		});
	</script>
@endsection