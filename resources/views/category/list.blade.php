@extends('layout')
@section('content')
          <!-- Page Heading -->
              
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Category List</h1>
            <a href="{{url('category/create')}}" class="d-none d-sm-inline-block btn btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> New Category</a>
          </div>

          @if(Request::session()->has('success'))
          <div class="alert alert-success">{{Request::session()->get('success')}}</div>
          @endif
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Category List</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Title</th>
                      <th>User</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Title</th>
                      <th>User</th>
                      <th>Action</th>                      
                    </tr>
                  </tfoot>
                  <tbody>
                    @php $i = 0; @endphp
                    @foreach($list as $row)
                    @php $i++; @endphp
                    <tr>
                      <td>{{$i}}</td>
                      <td>{{ $row->title }}</td>                      
                      <td>{{ $row->user->name }}</td>                      
                      <td class="text-center">
                        <a href="#" data-toggle="modal" data-delete="{{ $row->id }}" data-target="#deleteModal" class="btn btn-danger btn-icon-split btn-sm">
                          <span class="icon text-white-50">
                            <i class="fas fa-trash"></i>
                          </span>
                          <span class="text">Delete</span>
                        </a>
                        <a href="{{url('category/edit/'.$row->id)}}" class="btn btn-info btn-icon-split btn-sm">
                          <span class="icon text-white-50">
                            <i class="fas fa-edit"></i>
                          </span>
                          <span class="text">Edit</span>
                        </a>
                      </td>
                    </tr>
                    @endforeach
            
                  </tbody>
                </table>
              </div>
            </div>
          </div>


          <!-- Delete Modal-->
          <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="{{url('category')}}" method="POST"> 
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Delete?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    Select "Delete" below if you are ready to delete this data.
                    <div class="form-group">
                      <input type="hidden" class="form-control" name="id" value="">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" >Delete</butotn>
                  </div>
                </form>
              </div>
            </div>
          </div>
@endsection


@section('js')
<script>
  $('#deleteModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var recipient = button.data('delete')
    var modal = $(this)
    modal.find('.modal-body input').val(recipient)
  })
</script>
@endsection